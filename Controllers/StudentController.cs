using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using LoggingAndException.Models;
using LoggingAndException.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using StudentApiUsingSql.Models;

namespace StudentApiUsingSql.Controllers {
    [ApiController]
    [Route ("api/[controller]")]
    public class StudentController : Controller {
        IConfiguration Configure;

        public StudentController (IConfiguration configuration) {
            Configure = configuration;
        }

        [HttpGet ("list")]
        public IActionResult Get () {
            string query = @"select * from Students";
            try {
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult (table);
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpGet ("single/{id}")]
        public ActionResult GetSingleById (int id) {
            string query = @"select * from Students where Id ='" + id + "'";
            try {
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult (table);
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpPost ("create")]
        public ActionResult Post (StudentModel student) {
            string query = @"insert into  Students values('" + student.Name + @"','" + student.Department + @"','" + student.Gender + @"','" + student.DateOfBirth + @"','" + student.MarksPercentage + @"')";
            try {
                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Data Created Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpPut ("update/{id}")]
        public ActionResult Put (StudentModel student, int id) {
            try {
                string query = @"update Students set Name ='" + student.Name + @"',Department='" + student.Department + @"',Gender='" + student.Gender + @"',DateOfBirth='" + student.DateOfBirth + @"',MarksPercentage='" + student.MarksPercentage + @"'where Id='" + id + "'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Data Updated Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpDelete ("delete/{id}")]
        public ActionResult Delete (int id) {
            try {
                string query = @"delete from Students where Id='" + id + @"'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Requested Data has been deleted Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }
    }
}