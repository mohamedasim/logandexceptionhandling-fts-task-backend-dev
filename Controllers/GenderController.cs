using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using LoggingAndException.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using StudentApiUsingSql.Models;
namespace StudentApiUsingSql.Controllers {
    [ApiController]
    [Route ("api/[controller]")]
    public class GenderController : Controller {
        IConfiguration Configure;

        public GenderController (IConfiguration configuration) {
            Configure = configuration;
        }

        [HttpGet ("list")]
        public ActionResult Get () {
            try {
                string query =@"select * from Gender";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult (table);
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpGet ("single/{id}")]
        public ActionResult GetSingleById (int id) {
            try {
                string query = @"select * from Gender where Id ='" + id + "'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult (table);
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpPost ("create")]
        public ActionResult Post (GenderModel gender) {
            try {
                string query = @"insert into  Gender values('" + gender.Name + @"')";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Data Created Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpPut ("update/{id}")]
        public ActionResult Put (GenderModel gender, int id) {
            try {
                string query = @"update Gender set Name ='" + gender.Name + @"'where Id='" + id + "'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Data Updated Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }

        [HttpDelete ("delete/{id}")]
        public ActionResult Delete (int id) {
            try {
                string query = @"delete from Gender where Id='" + id + @"'";

                DataTable table = new DataTable ();
                string sqlDatasource = Configure.GetConnectionString ("DefaultConnection");
                SqlDataReader dataReader;
                using (SqlConnection connection = new SqlConnection (sqlDatasource)) {
                    connection.Open ();
                    using (SqlCommand cmd = new SqlCommand (query, connection)) {
                        dataReader = cmd.ExecuteReader ();
                        table.Load (dataReader);
                    }
                }
                string URL = Request.Scheme + "://" + HttpContext.Request.Host.Value + Request.Path.Value;
                string Verb = Request.Method;
                string Query = Request.QueryString.Value;
                ICollection<string> Headers = Request.Headers.Keys;
                CommonMethod.ApiLog (URL, Verb, Query, Headers);
                return new JsonResult ("your Requested Data has been deleted Successfully!");
            } catch (Exception e) {
                string StackTrace = e.StackTrace.ToString ();
                string Message = e.Message.ToString ();
                CommonMethod.ExceptionLog (Message, StackTrace);
                return StatusCode (500, e);
            }
        }
    }
}