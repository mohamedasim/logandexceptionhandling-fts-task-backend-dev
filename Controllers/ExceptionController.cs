using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Mvc;
namespace LoggingAndException.Controllers {
    [ApiController]
    [Route ("api")]
    public class ExceptionController : Controller {
        [HttpGet ("exception-log/{date}")]
        public ActionResult GetExceptionLog (string date) {
            string path = @"E:\FTSAsim\FTS-Tasks\backend-dev\LoggingAndException\ExceptionLogs\Exception" + date + ".txt";
            string[] readText = System.IO.File.ReadAllLines (path);
            foreach (string s in readText) {
                Console.WriteLine (s);
            }
            return Ok (string.Join ("\n", readText ));
        }
        [HttpGet ("api-log/{date}")]
        public ActionResult GetApiLog (string date) {
            string path = @"E:\FTSAsim\FTS-Tasks\backend-dev\LoggingAndException\ApiLogs\Api" + date + ".txt";
            string[] readText = System.IO.File.ReadAllLines (path);
            foreach (string s in readText) {
                Console.WriteLine (s);
            }
            return Ok (string.Join ("\n", readText ));
        }
    }
}