using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using LoggingAndException.Models;
namespace LoggingAndException.Services {
    public class CommonMethod {
        public static void ExceptionLog (string Message,string StackTrace) {
            DateTime dateForFile = DateTime.Now;
            string dateString = dateForFile.ToString ("dd-MM-yyyy");
            string dateinString = dateForFile.ToString ("dd-MM-yyyy hh:mm tt");
            string path = @"E:\FTSAsim\FTS-Tasks\backend-dev\LoggingAndException\ExceptionLogs\Exception" + dateString + ".txt";
            if (!System.IO.File.Exists (path)) {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText (path)) {
                    sw.WriteLine ("Date and Time: "+dateinString);
                    sw.WriteLine("Message: "+Message);
                    sw.WriteLine("StackTrace: "+StackTrace);
                    sw.WriteLine("-----------------------------------------------------------------------");
                    sw.WriteLine("");
                }
            }
            else
            {
            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = System.IO.File.AppendText (path)) {
                sw.WriteLine ("Date and Time: "+dateinString);
                sw.WriteLine("Message: "+Message);
                sw.WriteLine("StackTrace: "+StackTrace);
                sw.WriteLine("-----------------------------------------------------------------------------");
                sw.WriteLine("");
            }
            }
        }
        public static void ApiLog (string URL,string Verb,string Query,ICollection<string> Headers) {
            DateTime dateForFile = DateTime.Now;
            string dateString = dateForFile.ToString ("dd-MM-yyyy");
            string dateinString = dateForFile.ToString ("dd-MM-yyyy hh:mm tt");
            string path = @"E:\FTSAsim\FTS-Tasks\backend-dev\LoggingAndException\ApiLogs\Api" + dateString + ".txt";
            if (!System.IO.File.Exists (path)) {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText (path)) {
                    sw.WriteLine ("Date and Time: "+dateinString);
                    sw.WriteLine("URL: "+URL);
                    sw.WriteLine("Verb: "+Verb);
                    sw.WriteLine("Query: "+Query);
                    sw.WriteLine("Headers: ");
                    foreach(var data in Headers){
                        sw.WriteLine("key = "+data);
                    }
                    sw.WriteLine("-----------------------------------------------------------------------");
                    sw.WriteLine("");
                }
            }
            else
            {
            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = System.IO.File.AppendText (path)) {
                sw.WriteLine ("Date and Time: "+dateinString);
                sw.WriteLine("URL: "+URL);
                sw.WriteLine("Verb: "+Verb);
                sw.WriteLine("Query: "+Query);
                sw.WriteLine("Headers: ");
                    foreach(var data in Headers){
                        sw.WriteLine("key = "+data);
                    }
                sw.WriteLine("-----------------------------------------------------------------------------");
                sw.WriteLine("");
            }
            }
        }
    }
}